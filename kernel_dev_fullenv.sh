#!/bin/sh

echo "**************************************"
echo "The script is for Linux Kernel build"
echo "environment setting for Beaglebone Black."
echo "               by H.Yoshiki, 2016"
echo "**************************************"


echo ""
echo "first of all, make me superuser."
echo ""
su


#[1-1]kernel building preparation
sudo apt-get install -y git gcc-arm-linux-gnueabihf lzop libssl-dev g++-multilib zlib1g:i386


cd
mkdir kernel-build
cd kernel-build

wget ftp://ftp.denx.de/pub/u-boot/u-boot-latest.tar.bz2
tar -xjf u-boot-latest.tar.bz2
cd u-boot*
make sandbox_defconfig tools-only
sudo install tools/mkimage /usr/local/bin
cd ..

#[1-2]kernel tree choice
git clone git://github.com/beagleboard/linux.git
cd linux
git checkout 4.4  # <-visit repo above. choose the right branch 
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bb.org_defconfig 
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j4
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- uImage dtbs LOADADDR=0x80008000 -j4
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- modules

#[2-1]Qt cross-compile environment
wget https://download.qt.io/official_releases/qt/5.7/5.7.0/single/


??????????
