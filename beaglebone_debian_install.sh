#!/bin/sh

echo "**************************************"
echo "* Beaglebone Black SD Installer      *"
echo "* Debian Jessie - lxqt (kernel 4.4.1)*"
echo "*             by H.Yoshiki, Jul 2016 *"
echo "**************************************"
echo ""
echo ""
echo "image: [1]bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb"
echo "image: [2]bone-debian-8.4-lxqt-4gb-armhf-2016-05-13-4gb"
echo "image: [3]bone-debian-8.3-lxqt-4gb-armhf-2016-01-24-4gb"
echo "image: [9x] install without wget"
echo "install target: /dev/sdb"
echo ""

echo "input and select install image:"
read image_num

#echo "Are you ok?  Script automatically starts in 10 sec..."
#sleep 10s

cd
#mkdir bbb-image
#cd bbb-image
cd beaglebone


if [ "$image_num" -eq 1 ]
then
#For your reference:
#http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Jessie_Snapshot_lxqt
wget https://rcn-ee.com/rootfs/bb.org/testing/2016-06-19/lxqt-4gb/bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb.img.xz
sha256sum bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb*
# For security, confirm the key:
# eb83cf3dabdf406c2cbb67ab0aaf6cd2deb7978e408563db8ed68ef4f1394d46 bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb.img.xz
xzcat bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb.img.xz | sudo dd of=/dev/sdb

elif [ "$image_num" -eq 2 ]
then
#For your reference:
#https://debian.beagleboard.org/images/
wget https://debian.beagleboard.org/images/bone-debian-8.4-lxqt-4gb-armhf-2016-05-13-4gb.img.xz
sha256sum bone-debian-8.4-lxqt-4gb-armhf-2016-05-13-4gb.img.xz*
# For security, confirm the key:
#28d67e877497fb9e52fe605f2cbefdbaedaff23e9fa82e9ed2076ae375aa777f  bone-debian-8.4-lxqt-4gb-armhf-2016-05-13-4gb.img.xz
xzcat bone-debian-8.4-lxqt-4gb-armhf-2016-05-13-4gb.img.xz | sudo dd of=/dev/sdb

elif [ "$image_num" -eq 3 ]
then
#For your reference:
#https://debian.beagleboard.org/images/
wget https://debian.beagleboard.org/images/bone-debian-8.3-lxqt-4gb-armhf-2016-01-24-4gb.img.xz
xzcat bone-debian-8.3-lxqt-4gb-armhf-2016-01-24-4gb.img.xz | sudo dd of=/dev/sdb

elif [ "$image_num" -eq 91 ]
then
#http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Jessie_Snapshot_lxqt
echo "working on image [1] w/o wget..."
xzcat bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb.img.xz | sudo dd of=/dev/sdb

elif [ "$image_num" -eq 92 ]
then
#For your reference:
#https://debian.beagleboard.org/images/
echo "working on image [2] w/o wget..."
xzcat bone-debian-8.4-lxqt-4gb-armhf-2016-05-13-4gb.img.xz | sudo dd of=/dev/sdb

elif [ "$image_num" -eq 93 ]
then
#For your reference:
#https://debian.beagleboard.org/images/
echo "working on image [3] w/o wget..."
xzcat bone-debian-8.3-lxqt-4gb-armhf-2016-01-24-4gb.img.xz | sudo dd of=/dev/sdb


else
echo "bad request: no images with number $image_num"

fi

echo "beaglebone_debian_install.sh finished!"
