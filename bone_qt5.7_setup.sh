#!/bin/sh

echo "**************************************"
echo "* Qt 5.7 setup script		   *"
echo "*             by H.Yoshiki, Jul 2016 *"
echo "**************************************"
echo ""
echo ""


cd
mkdir Qt-install
cd Qt-install
wget https://download.qt.io/official_releases/qt/5.7/5.7.0/single/qt-everywhere-opensource-src-5.7.0.tar.xz
unxz qt-everywhere-opensource-src-5.7.0-*.xz
tar xvf qt-everywhere-opensource-src-5.7.0-*.tar
cd qt-everywhere-opensource-src-5.7.0*
./configure
make
make install

echo "PATH=/usr/local/Qt-5.7/bin:$PATH" >> ~/.profile
echo "export PATH" >> ~/.profile


