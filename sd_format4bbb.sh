#!/bin/sh

echo "**************************************"
echo "* SD Formatter for Beaglebone Black  *"
echo "*             by H.Yoshiki, Jul 2016 *"
echo "**************************************"
echo ""
echo "target: /dev/sdb"
echo ""
echo "Are you ok?  Script automatically starts in 10 sec..."

dmesg
sleep 10s

cd

#For your reference:
#http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Jessie_Snapshot_lxqt
wget https://rcn-ee.com/rootfs/bb.org/testing/2016-06-19/lxqt-4gb/bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb.img.xz
sha256sum bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb*

# For security, confirm the key:
# eb83cf3dabdf406c2cbb67ab0aaf6cd2deb7978e408563db8ed68ef4f1394d46 bone-debian-8.5-lxqt-4gb-armhf-2016-06-19-4gb.img.xz

unxz bone-debian-8.5-*.img.xz
sudo dd if=bone-debian-8.5-*.img of=/dev/sdb

