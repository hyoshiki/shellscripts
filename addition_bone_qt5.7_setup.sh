#!/bin/sh

echo "**************************************"
echo "* Qt 5.7 setup script		   *"
echo "*             by H.Yoshiki, Jul 2016 *"
echo "**************************************"
echo ""
echo ""

echo "this is for the continue..."

#./configure
#make
make install

###############################################################
# build qt3d
cd qt3d
make
make install
cd ..

###############################################################
# build qtactiveqt
cd qtactiveqt
make
make install
cd ..

###############################################################
# build qtcanvas3d
cd qtcan*
make
make install
cd ..

###############################################################
# build qtcharts
cd qtcha*
make
make install
cd ..

###############################################################
# build qtconnectivity
cd qtconn*
make
make install
cd ..

###############################################################
# build qtdatavis3d
cd qtdat*
make
make install
cd ..

###############################################################
# build qtdeclarative
cd qtdec*
make
make install
cd ..

###############################################################
# build qtdoc
cd qtdoc
make
make install
cd ..

###############################################################
# build qtgraphicaleffect
cd qtgra*
make
make install
cd ..

###############################################################
# build qtmultimedia
cd qtmul*
make
make install
cd ..

###############################################################
# build qtpurchasing
cd qtpu*
make
make install
cd ..

###############################################################
# build qtquickcontrols 1/2
cd qtq*s
make
make install
cd ..
cd qtq*s2
make
make install
cd ..

###############################################################
# build qtscript
cd qtscr*
make
make install
cd ..

###############################################################
# build qtscxml
cd qtscx*
make
make install
cd ..


###############################################################
# build qtsensors
cd qtsen*
make
make install
cd ..

###############################################################
# build qtserialbus/serialport
cd qtserialb*
make
make install
cd ..
cd qtserialp*
make
make install
cd ..

###############################################################
# build qtsvg
cd qtsvg
make
make install
cd ..

###############################################################
# build qttools
cd qtto*
make
make install
cd ..

###############################################################
# build qttranslations
cd qttra*
make
make install
cd ..

###############################################################
# build qtvirtualkeyboard
cd qtvir*
make
make install
cd ..

###############################################################
# build qtwayland
cd qtwa*
make
make install
cd ..

###############################################################
# build qtwebengine
cd qtwebe*
make
make install
cd ..

###############################################################
# build qtx11extras
cd qtx1*
make
make install
cd ..

###############################################################
# build qtxmlpatterns
cd qtxm*
make
make install
cd ..

