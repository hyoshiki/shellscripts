#!/bin/sh

echo "**************************************"
echo "* Yocto setup script	           *"
echo "*             by H.Yoshiki, Jul 2016 *"
echo "**************************************"
echo ""
echo ""
echo "[1]  full-scratch setup"
echo "[9x] setup without sth"
echo ""

echo "input and select setup mode:"
read mode
cd

if [ "$mode" -eq 1 ]
then
#For your reference:
sudo apt-get install -y build-essential chrpath diffstat gawk git libncurses5-dev pkg-config subversion texi2html texinfo libsda1.2-dev libgl1-mesa-dev

mkdir yocto
cd yocto
git clone -b krogoth git://git.yoctoproject.org/poky.git poky-krogoth
cd poky-krogoth
git clone -b krogoth git://git.openembedded.org/meta-openembedded
git clone -b krogoth https://github.com/meta-qt5/meta-qt5.git
git clone -b krogoth git://github.com/jumpnow/meta-bbb
cd
source yocto/poky-krogoth/oe-init-build-env yocto/poky-krogoth/build
cd
cd yocto/poky-krogoth
cp meta-bbb/conf/local.conf-sample build/conf/local.conf
cp meta-bbb/conf/bblayers.conf-sample build/conf/bblayers.conf


elif [ "$mode" -eq 2 ]
then
echo "mode 2"


elif [ "$image_num" -eq 3 ]
then
echo "mode 3"

else
echo "bad request: no images with number $image_num"

fi

echo "yocto_setup.sh finished!"
